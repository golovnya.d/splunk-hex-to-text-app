#!/usr/bin/env python
import sys
from splunklib.searchcommands import dispatch, StreamingCommand, Configuration, Option, validators


@Configuration()
class hextotext(StreamingCommand):
    hex_field = Option(require=True)

    def stream(self, events):
        # Put your event transformation code here
        for event in events:
            try:
                byte_array = bytearray.fromhex(event[self.hex_field])
            except:
                event['hextotext'] = None
                pass
            else:
                event['hextotext'] = str(byte_array)
            yield event


dispatch(hextotext, sys.argv, sys.stdin, sys.stdout, __name__)