# Splunk HEX to Text app

Splunk application to convert HEX data to text.

Usage:
| hextotext hex_field=packet
